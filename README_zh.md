# snakeyaml

## 简介
> snakeyaml语言解析功能库。
> 支持： Parse & Stringify, YAML Documents等。

![](screenshot/screenshot.gif)

## 下载安装
```shell
ohpm install yaml@1.10.2
```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

### YAMI.stringify
 ```
YAML.stringify([true, false, 'maybe', null])
// `- true
// - false
// - maybe
// - null
// `
 ```
### YAML.parse
 ```
YAML.parse('3.14159')
// 3.14159
 ```

### Documents
 ```
const doc = new Document(['some', 'values', { balloons: 99 }])
doc.commentBefore = ' A commented document'

String(doc)
// # A commented document
//
// - some
// - values
// - balloons: 99
 ```

## 接口说明
1. parse解析
   `YAML.parse()`
2. stringify解析
   `YAML.stringify()`
3. parseDocument解析
   `YAML.parseDocument()`
4. Document解析
   `YAML.Document`

## 约束与限制
在下述版本验证通过：

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)

- OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构
````
|---- snakeyaml 
|     |---- entry  # 示例代码文件夹
|     |---- README.md     # 安装使用方法
|     |---- README_zh.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/snakeyaml/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/snakeyaml/pulls) 。

## 开源协议
本项目基于 [ISC License](https://gitee.com/openharmony-sig/snakeyaml/blob/master/LICENSE) ，请自由地享受和参与开源。