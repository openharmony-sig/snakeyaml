/*
* Copyright (c) 2022 Huawei Device Co., Ltd.

* Permission to use, copy, modify, and/or distribute this software for any purpose
* with or without fee is hereby granted, provided that the above copyright notice
* and this permission notice appear in all copies.

* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
* REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
* INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
* OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
* TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
* THIS SOFTWARE.
*/

import  YAML  from 'yaml'

@Entry
@Component
struct YamlStringify {
  @State yamlStringifyDataList: Array<string> = ["", "", ""]

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center}) {
      Text("YAML Stringify").fontSize(25).fontColor(Color.Gray).padding(20)
      Column({ space: 10 }) {
        Text("original data: 3.14159").fontSize(15)
        Text("YAMI.stringify data：" + this.yamlStringifyDataList[0]).fontSize(15)
      }.alignItems(HorizontalAlign.Start).width("100%").padding({ left: 15, bottom: 25 })

      Column({ space: 10 }) {
        Text("original data: [true, false, 'maybe', null]").fontSize(15)
        Text("YAMI.stringify data：\n" + this.yamlStringifyDataList[1]).fontSize(15)
      }.alignItems(HorizontalAlign.Start).width("100%").padding({ left: 15, bottom: 25 })

      Column({ space: 10 }) {
        Text("original data: { number: 3, plain: 'string', block: 'two\nlines\n' }").fontSize(15)
        Text("YAMI.stringify data：\n" + this.yamlStringifyDataList[2]).fontSize(15)
      }.alignItems(HorizontalAlign.Start).width("100%").padding({ left: 15, bottom: 25 })

      Button("YAML Stringify", { type: ButtonType.Normal, stateEffect: true }).borderRadius(8).width("50%")
        .onClick(() => {
          this.yamlStringifyDataList[0] = YAML.stringify(3.14159)
          this.yamlStringifyDataList[1] = YAML.stringify([true, false, 'maybe', null])
          this.yamlStringifyDataList[2] = YAML.stringify({ number: 3, plain: 'string', block: 'two\nlines\n' })
          console.info('YAML Stringify---'+JSON.stringify(this.yamlStringifyDataList))
        })
    }.width("100%")
  }
}