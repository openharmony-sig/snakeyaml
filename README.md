# snakeyaml

## Introduction
> SnakeYAML is a library for parsing and generating YAML documents.
> It provides **parse()**, **stringify()**, and **YAML.Document**.

![](screenshot/screenshot.gif)

## How to Install
```shell
ohpm install yaml@1.10.2
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage_en.md).

## How to Use

### YAMI.stringify
 ```
YAML.stringify([true, false, 'maybe', null])
// `- true
// - false
// - maybe
// - null
// `
 ```
### YAML.parse
 ```
YAML.parse('3.14159')
// 3.14159
 ```

### Documents
 ```
const doc = new Document(['some', 'values', { balloons: 99 }])
doc.commentBefore = ' A commented document'

String(doc)
// # A commented document
//
// - some
// - values
// - balloons: 99
 ```

## Available APIs
1. parse
   `YAML.parse()`
2. stringify
   `YAML.stringify()`
3. parseDocument
   `YAML.parseDocument()`
4. Document
   `YAML.Document`

## Constraints
This project has been verified in the following version:

- DevEco Studio: 4.1 Canary (4.1.3.317)

- OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure
````
|---- snakeyaml 
|     |---- entry  # Sample code
|     |---- README.md     # Readme       
|     |---- README_zh.md  # Readme               
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/snakeyaml/issues) or a [PR](https://gitee.com/openharmony-sig/snakeyaml/pulls).

## License
This project is licensed under [ISC LICENSE](https://gitee.com/openharmony-sig/snakeyaml/blob/master/LICENSE).
